var debounce = require('./utils').debounce,
    mori = require('mori');

// atom state

var state;
var atom;

// some utils

var slice = [].slice;

function wrap(op, args) {
  return mori[op].apply(mori, [atom.get()].concat(args));
}

// simple publisher

var notifySwap = debounce(function(state) {
    var changedAttrs = resetChangedAttrs(),
      listenersLength = listeners.length,
      i;
    for (i = listenersLength; i--;) {
      //why is decremental?
      notifyToListener(listeners[i], changedAttrs);
    }
  }, 10),

  notifyToListener = function (item, changedAttrs) {
    var callbacks = item.callbacks,
      itemAttr;
    for (itemAttr in callbacks) {
      if (callbacks.hasOwnProperty(itemAttr) && isChangedAttrsMatch(itemAttr, changedAttrs)) {
        callbacks[itemAttr].call(item.ctx, state);
      }
    }
  },

  /**
  changedAttrs:
  Since we run notifySwap with debounce, we have to store changed attributes
  to ensure notice all listeners
  **/

  changedAttrs = [];

  newChangedAttr = function (attr) {
    attr = attr.join('.');
    if (changedAttrs.indexOf(attr) < 0) {
      changedAttrs.push(attr);
    }
  },

  resetChangedAttrs = function () {
    var attrs = changedAttrs;
    changedAttrs = [];
    return attrs;
  },

  isChangedAttrsMatch = function (listenersAttr, changedAttrs) {
    //aqui falta comprobar si el proton modificado es ascendete o descentente de listenersAttr
    //por eso estamos tratando listenersAttr y changedAttrs como strings
    return changedAttrs.indexOf(listenersAttr) >= 0;
  };

  /**
  listeners = [{
    ctx: ctx,
    callbacks: {
      'foo.child': 'fn',
      'bar.otherChild': 'fn2'
    }
  }]
  **/

  listeners = [],

  //check listeners to be sure that all callbacks are contexts functions
  getValidListeners = function (ctx, callbacks) {
    var validCallbacks = {},
      callback,
      attr;
    for (attr in callbacks) {
      if (callbacks.hasOwnProperty(attr)) {
        callback = callbacks[attr];
        if (typeof callback === 'string') {
          callback = ctx[callback];
        }
        if (typeof callback === 'function') {
          validCallbacks[attr] = callback;
        }
      }
    }
    return validCallbacks;
  };

// the atom per se

atom = {
  get: function() {
    return state;
  },
  swap: function(newState, attr) {
    this.silentSwap(newState);
    newChangedAttr(attr);
    notifySwap(state);
    return newState;
  },
  silentSwap: function(newState) {
    state = newState;
    return newState;
  },
  addChangeListener: function(ctx, callbacks) {
    listeners.push({
      ctx: ctx,
      callbacks: getValidListeners(ctx, callbacks)
    });
  },
  removeChangeListener: function (ctx) {
    var result = []; //TODO: probar este metodo
    listeners.forEach(function (listener) {
      if (listener.ctx !== ctx) {
        result.push(listener);
      }
    });
    listeners = result;
  },
  // extend it with some mori ops
  getIn: function() {
    return wrap('getIn', slice.call(arguments));
  },
  assocIn: function(attr) {
    return atom.swap(wrap('assocIn', slice.call(arguments)), attr);
  },
  updateIn: function(attr) {
    return atom.swap(wrap('updateIn', slice.call(arguments)), attr);
  },
  silentAssocIn: function() {
    return atom.silentSwap(wrap('assocIn', slice.call(arguments)));
  },
  silentUpdateIn: function() {
    return atom.silentSwap(wrap('updateIn', slice.call(arguments)));
  }
};

module.exports = atom;
