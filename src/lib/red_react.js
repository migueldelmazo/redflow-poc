var React = require('react'),
  atom = require('./atom_state');

//wrap React.createClass
React._createClass = React.createClass;

React.createClass = function (spec) {

  if (spec.hasOwnProperty('atomListeners')) {

    //wrap componentWillMount method
    if (spec.componentWillMount) {
      spec._componentWillMount = spec.componentWillMount;
    }
    spec.componentWillMount = function () {
      this.addListenersToAtom();
      if (this._componentWillMount) {
        this._componentWillMount();
      }
    }

    //wrap componentWillUnmount method
    if (spec.componentWillUnmount) {
      spec._componentWillUnmount = spec.componentWillUnmount;
    }
    spec.componentWillUnmount = function () {
      this.removeListenersFromAtom();
      if (this._componentWillUnmount) {
        this._componentWillUnmount();
      }
    }

    //atom listeners
    spec.addListenersToAtom = function () {
      atom.addChangeListener(this, this.atomListeners);
    }
    spec.removeListenersFromAtom = function () {
      atom.removeChangeListener(this);
    }
  }

  spec.forceRender = function (state) {
    this.forceUpdate();
  }

  return React._createClass(spec);
}

module.exports = React;
